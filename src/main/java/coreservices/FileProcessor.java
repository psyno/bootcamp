package coreservices;

import coreservices.supportedfileextensions.CSVExtension;
import coreservices.supportedfileextensions.XMLExtension;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * The type File processor.
 */
class FileProcessor {
    /**
     * An ArrayList of orders readed from all files.
     */
    private ArrayList<Order> orders;

    /**
     * Not used; prevents instantation.
     */
    private FileProcessor() {
    }

    /**
     * Instantiates a new File processor.
     *
     * @param filePaths the file paths
     */
    FileProcessor(final String[] filePaths) {
        this.orders = new ArrayList<>();
        readOrders(filePaths);
    }

    /**
     * Reads orders from files.
     *
     * @param filePaths the list of files to be proceeded
     */
    void readOrders(final String[] filePaths) {
        for (String fileName : filePaths) {
            File tmpFile = new File(fileName);
            System.out.println(">>>\tProcessing "
                    + tmpFile.getName() + " file\t<<<");
            if (tmpFile.isFile()) { // check if file exists
                switch (tmpFile.getName().split("\\.")[1].toLowerCase()) {
                    case "xml":
                        XMLExtension xmlExtension = new XMLExtension(tmpFile);
                        this.addWithoutDuplicates(xmlExtension.readFromFile());
                        break;
                    case "csv":
                        CSVExtension csvExtension = new CSVExtension(tmpFile);
                        this.addWithoutDuplicates(csvExtension.readFromFile());
                        break;
                    default:
                        System.out.println("File "
                                + fileName
                                + " has invalid file extension.");
                        break;
                }
                System.out.println(">>>\tFinishing "
                        + tmpFile.getName() + " file\t<<<");
            } else {
                System.out.println("File " + fileName + " doesn't exist.");
            }
        }
    }

    /**
     * Concacenate readed from file orders with
     * already readed in {@link #orders}.
     * Concatenation points duplicates and asks
     * user which record should be kept.
     *
     * @param list the list to be concatenated
     */
    void addWithoutDuplicates(final ArrayList<Order> list) {
        boolean duplicateNotFound = true;
        for (Order order : list) {
            for (Order alreadyAdded : orders) {
                duplicateNotFound = true;
                if (compare(order, alreadyAdded)) {
                    Scanner reader = new Scanner(System.in);
                    System.out.println("Duplicate found;"
                            + " ClientId=" + order.getQuantity()
                            + ", RequestId=" + order.getRequestId()
                            + ", Name=" + order.getName()
                            + "\n#1\tCurrently on list:\t" + alreadyAdded.getQuantity()
                            + " pcs @ " + alreadyAdded.getPrice()
                            + "\n#2\tTo be added:\t\t" + order.getQuantity()
                            + " pcs @ " + order.getPrice()
                            + "\nWhich position should we keep? (type 1 or 2)");
                    boolean showOptions = true;
                    String pickS;
                    int pick = 0;

                    while (showOptions) {
                        pickS = reader.next();
                        try {
                            pick = Integer.parseInt(pickS);
                            if (pick == 1 || pick == 2) {
                                showOptions = false;
                            } else {
                                System.out.println("Wrong number; Please try again:");
                            }
                        } catch (NumberFormatException e) {
                            System.out.println("Choice has to be number 1 or 2; "
                                    + "Please try again:");
                        }
                    }
                    if (pick == 2) {
                        alreadyAdded.setQuantity(String.valueOf(order.getQuantity()));
                        alreadyAdded.setPrice(String.valueOf(order.getPrice()));
                    }
                    duplicateNotFound = false;
                    break;
                }
            }
            if (duplicateNotFound) {
                order.setId(orders.size());
                orders.add(order);
            }
        }
    }

    /**
     * Compares orders. Compares orders to be added with currently on list
     * to avoid duplicates.
     *
     * @param order1 the 1st order to be compared
     * @param order2 the 2nd order to be compared
     */
    boolean compare(final Order order1, final Order order2) {
        return order1.getClientId().equals(order2.getClientId())
                && order1.getRequestId() == order2.getRequestId()
                && order1.getName().equals(order2.getName());
    }

    /**
     * Gets orders.
     *
     * @return the orders
     */
    final ArrayList<Order> getOrders() {
        return orders;
    }

    @Override
    public final String toString() {
        return "FileProcessed{" + "orders=" + orders + '}';
    }
}
