package coreservices.reportoutput;

import coreservices.App;

/**
 * The interface Report covers all executed queries in Database class.
 * New extensions should be listed in currentlySupported on {@link App#fileNameRequest()}
 * and filled in {@link coreservices.Database} switches.
 */
interface Report {
    /**
     * Shows total number of all orders.
     *
     * @param total the total
     */
    void showTotalOrders(int total);

    /**
     * Shows total number of orders specific client.
     *
     * @param total  the total
     * @param client the client
     */
    void showTotalOrders(int total, String client);

    /**
     * Shows total value of all orders.
     *
     * @param value the value
     */
    void showTotalValue(double value);

    /**
     * Shows total value of all order for specific client.
     *
     * @param value  the value
     * @param client the client
     */
    void showTotalValue(double value, String client);

    /**
     * Shows list of orders.
     * <p>
     * Entry decides if record should be treated
     * as header or as regular row in table.
     *
     * @param entry     the entry
     * @param clientId  the client id
     * @param requestId the request id
     * @param name      the name
     * @param quantity  the quantity
     * @param price     the price
     */
    void showAllOrders(boolean entry, String clientId, long requestId,
                       String name, int quantity, double price);

    /**
     * Show average value of all orders.
     *
     * @param average the average
     */
    void showAvgValueOrder(double average);

    /**
     * Show average value of all order for specific client.
     *
     * @param average the average
     * @param client  the client
     */
    void showAvgValueOrder(double average, String client);
}
