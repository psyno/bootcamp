package coreservices.reportoutput;

/**
 * The type On screen report covers all necessary method
 * to show in console executed queries in {@link coreservices.Database}.
 */
public final class OnScreenReport implements Report {
    @Override
    public void showTotalOrders(final int total) {
        System.out.println("Total number of orders is: " + total);
    }

    @Override
    public void showTotalOrders(final int total, final String client) {
        System.out.println("Client with ID \""
                + client + "\" got "
                + total + (total == 1 ? " order." : " orders."));
    }

    @Override
    public void showTotalValue(final double value) {
        System.out.println("Total value of orders is " + value);
    }

    @Override
    public void showTotalValue(final double value, final String client) {
        System.out.println("Total value of orders for client \""
                + client + "\" is " + value);
    }

    @Override
    public void showAllOrders(final boolean entry, final String clientId, final long requestId,
                              final String name, final int quantity, final double price) {
        if (!entry) {
            System.out.format("%-15s%-15s%-30s%-15s%-15s%n",
                    "ClientId", "RequestId", "Name", "Quantity", "Price");
        } else {
            System.out.format("%-15s%-15d%-30s%-15d%-15.2f%n", clientId, requestId, name, quantity, price);
        }
    }

    @Override
    public void showAvgValueOrder(final double average) {
        System.out.println("Average order value is " + average);
    }

    @Override
    public void showAvgValueOrder(final double average, final String client) {
        System.out.println("Average client \""
                + client + "\" order value is " + average);
    }
}
