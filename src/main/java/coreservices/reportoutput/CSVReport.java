package coreservices.reportoutput;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;

/**
 * The type CSV report covers all necessary method
 * to save in CSV file executed queries in {@link coreservices.Database}.
 */
public final class CSVReport implements Report {
    /**
     * Object to be written.
     */
    private CSVWriter csvWriter;
    /**
     * Contains row of records to be executed.
     */
    private String[] entries;

    /**
     * Instantiates a new Csv report.
     *
     * @param filename the filename
     */
    public CSVReport(final String filename) {
        try {
            csvWriter = new CSVWriter(new FileWriter(filename), ',');
            csvWriter.writeNext(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets csv writer.
     *
     * @return the csv writer
     */
    public CSVWriter getCsvWriter() {
        return csvWriter;
    }

    @Override
    public void showTotalOrders(final int total) {
        entries = new String[]{"Total number of orders", Integer.toString(total)};
        csvWriter.writeNext(entries, false);
    }

    @Override
    public void showTotalOrders(final int total, final String client) {
        entries = new String[]{"ClientId", client};
        csvWriter.writeNext(entries, false);
        entries = new String[]{"Total number of orders", Integer.toString(total)};
        csvWriter.writeNext(entries, false);
    }

    @Override
    public void showTotalValue(final double value) {
        entries = new String[]{"Total value of orders", Double.toString(value)};
        csvWriter.writeNext(entries, false);
    }

    @Override
    public void showTotalValue(final double value, final String client) {
        entries = new String[]{"ClientId", client};
        csvWriter.writeNext(entries, false);
        entries = new String[]{"Total value of orders", Double.toString(value)};
        csvWriter.writeNext(entries, false);
    }

    @Override
    public void showAllOrders(final boolean entry, final String clientId, final long requestId,
                              final String name, final int quantity, final double price) {
        if (!entry) {
            entries = new String[]{"ClientId", "RequestId", "Name", "Quantity", "Price"};
            csvWriter.writeNext(entries, false);
        } else {
            entries = new String[]{clientId, Long.toString(requestId),
                    name, Integer.toString(quantity), Double.toString(price)};
            csvWriter.writeNext(entries, false);
        }
    }

    @Override
    public void showAvgValueOrder(final double average) {
        entries = new String[]{"Average order value is ", Double.toString(average)};
        csvWriter.writeNext(entries, false);
    }

    @Override
    public void showAvgValueOrder(final double average, final String client) {
        entries = new String[]{"ClientId", client};
        csvWriter.writeNext(entries, false);
        entries = new String[]{"Average order value is ", Double.toString(average)};
        csvWriter.writeNext(entries, false);
    }
}
