/**
 * Subpackage of coreservices that provides supported output methods
 * for queries executed in {@link coreservices.Database}.
 */

package coreservices.reportoutput;
