package coreservices;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * The Main App.
 */
public class App {
    /**
     * Main.
     * <p>
     * On run args are taken as filenames of files with orders
     * to be proceeded.
     *
     * @param args filenames
     */
    public static void main(final String[] args) {
        if (args.length == 0) {
            System.err.println("No arguments provided; Session terminated.");
            System.exit(1);
        }
        FileProcessor fp = new FileProcessor(args);
        if (fp.getOrders().size() == 0) {
            System.err.println("None of the provided files contain orders; "
                    + "Session terminated.");
            System.exit(1);
        }

        boolean showMenu = true;
        boolean dontValidatePick = true;
        int pick = -1;
        String pickS;
        Scanner reader = new Scanner(System.in);

        System.out.println("All added orders:");
        System.out.println(fp);

        Database.openConnection();
        Database.dropDatabase();
        Database.createDatabase();
        Database.insertDatabase(fp.getOrders());

        while (showMenu) {
            System.out.println("=============================================\n"
                    + "What would you like to do:\n"
                    + "1.\tShow total amount of orders\n"
                    + "2.\tShow total amount of orders for specific client\n"
                    + "3.\tShow total value of orders\n"
                    + "4.\tShow total value of orders for specific client\n"
                    + "5.\tShow list of orders\n"
                    + "6.\tShow list of orders for specific client\n"
                    + "7.\tShow average value of orders\n"
                    + "8.\tShow average value of orders for specific client\n\n"
                    + "0.\tExit");

            while (dontValidatePick) {
                pickS = reader.next();
                try {
                    pick = Integer.parseInt(pickS);
                    if (-1 < pick && pick < 9) {
                        dontValidatePick = false;
                    } else {
                        System.out.println("Wrong number; Please try again:");
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Choice has to be number; "
                            + "Please try again:");
                }
            }

            dontValidatePick = true;

            if (pick != 0) {
                saveToFile();
            }

            switch (pick) {
                case 1:
                    Database.showTotalOrders();
                    break;
                case 2:
                    Database.showTotalOrders(clientRequest());
                    break;
                case 3:
                    Database.showTotalValue();
                    break;
                case 4:
                    Database.showTotalValue(clientRequest());
                    break;
                case 5:
                    Database.showAllOrders();
                    break;
                case 6:
                    Database.showAllOrders(clientRequest());
                    break;
                case 7:
                    Database.showAvgValueOrder();
                    break;
                case 8:
                    Database.showAvgValueOrder(clientRequest());
                    break;
                default:
                    showMenu = false;
                    break;
            }
        }
        Database.dropDatabase();
        Database.closeConnection();
    }

    /**
     * Asks user if the report should be only
     * shown on the screen or should be saved to file.
     * <p>
     * Prevents from wrong misschoices.
     */
    static void saveToFile() {
        System.out.println("Would you like to show results "
                + "or save them as file? (s/f)");
        String pickS = new Scanner(System.in).next();
        while (!pickS.equals("s") && !pickS.equals("f")) {
            System.out.println("Wrong choice; please pick "
                    + "s for show or "
                    + "f for save to file:");
            pickS = new Scanner(System.in).next();
        }

        if (pickS.equals("s")) {
            Database.setToFile("show");
        } else {
            String filename = fileNameRequest();
            switch (filename.split("\\.")[1]) {
                case "csv":
                    Database.setCsvReport(filename);
                    Database.setToFile(filename.split("\\.")[1]);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Ask user for filename of file where results will be saved.
     * Validates if file exists; if so prevents overwriting.
     *
     * @return filename
     */
    static String fileNameRequest() {
        final ArrayList<String> currentlySupported = new ArrayList<>(Arrays.asList("csv"));

        System.out.println("Please enter file name with extension (ex. file.csv):");
        System.out.println("Currently supported files: " + currentlySupported);
        String filename = new Scanner(System.in).next();
        String overwritePick;
        boolean fileOverwrite = false;

        while (!filename.contains(".")
                || !currentlySupported.contains(filename.split("\\.")[1].toLowerCase())
                || (new File(filename).exists() && !fileOverwrite)) {
            fileOverwrite = false;
            if (!filename.contains(".")) {
                System.out.println("No extension found. Please enter file name with extension (ex. file.csv):");
                System.out.println("Currently supported files: " + currentlySupported);
                filename = new Scanner(System.in).next();
            } else if (!currentlySupported.contains(filename.split("\\.")[1].toLowerCase())) {
                System.out.println("Extension not supported. Please enter file name with supported extension:");
                System.out.println("Currently supported files: " + currentlySupported);
                filename = new Scanner(System.in).next();
            } else {
                System.out.println("File already exists; should we overwrite? (y/n)");
                overwritePick = new Scanner(System.in).next();
                while (!overwritePick.equals("y") && !overwritePick.equals("n")) {
                    System.out.println("Wrong choice; please pick "
                            + "y for yes or "
                            + "n for no:");
                    overwritePick = new Scanner(System.in).next();
                }
                if (overwritePick.equals("y")) {
                    fileOverwrite = true;
                } else {
                    System.out.println("Please enter different file name with extension (ex. file.csv):");
                    System.out.println("Currently supported files: " + currentlySupported);
                    filename = new Scanner(System.in).next();
                }
            }
        }
        return filename;
    }

    /**
     * Ask user for clientId to filter data for specific request.
     *
     * @return clientId to be proceeded
     */
    static String clientRequest() {
        System.out.println("Please enter ClientId:");
        return new Scanner(System.in).next();
    }

    /**
     * Not used; prevents instantation.
     */
    private App() {
    }
}
