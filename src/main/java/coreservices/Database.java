package coreservices;

import coreservices.reportoutput.CSVReport;
import coreservices.reportoutput.OnScreenReport;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The type Database.
 * <p>
 * After adding new report extensions switches should be filled in.
 */
public final class Database {
    /**
     * The type String str. Contains database address.
     */
    private static final String str = "jdbc:sqlite:memory:ordersdb";
    /**
     * The type Connection connection.
     */
    private static Connection connection;
    /**
     * The type Statement statement.
     */
    private static Statement statement;
    /**
     * The type ResultSet resultSet.
     */
    private static ResultSet resultSet;
    /**
     * The type OnScreenReport onScreenReport.
     * Responsible for showing results on screen.
     */
    private static final OnScreenReport onScreenReport = new OnScreenReport();
    /**
     * The type CSVReport csvReport.
     * Responsible for saving results to CSV file.
     */
    private static CSVReport csvReport;
    /**
     * The type boolean toFile.
     * Determinates if result will be shown or saved to file.
     */
    private static String toFile = null;

    /**
     * Not used; prevents instantation.
     */
    private Database() {
    }

    /**
     * Sets csv report.
     *
     * @param report the report
     */
    static void setCsvReport(final String report) {
        Database.csvReport = new CSVReport(report);
    }

    /**
     * Sets to file.
     *
     * @param file the file
     */
    static void setToFile(final String file) {
        Database.toFile = file;
    }

    // Connections section

    /**
     * Open connection.
     */
    static void openConnection() {
        try {
            connection = DriverManager.getConnection(str);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close connection.
     */
    static void closeConnection() {
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //On-database operations

    /**
     * Create database.
     */
    static void createDatabase() {
        try {
            statement.execute("CREATE TABLE orders (\n"
                    + "   Id integer NOT NULL CONSTRAINT "
                    + "   orders_pk PRIMARY KEY,\n"
                    + "   ClientId varchar(" + Order.getMaxIdLength() + "),\n"
                    + "   RequestId bigint,\n"
                    + "   Name varchar(" + Order.getMaxNameLength() + "),\n"
                    + "   Quantity integer,\n"
                    + "   Price double\n"
                    + ");");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Insert database.
     *
     * @param orders the orders
     */
    static void insertDatabase(final ArrayList<Order> orders) {
        try {
            String query;
            final String into = "INSERT INTO orders "
                    + "(Id, ClientId, RequestId, Name, Quantity, Price) "
                    + "\nVALUES ";
            for (Order order : orders) {
                query = into + "("
                        + order.getId() + ",'"
                        + order.getClientId() + "',"
                        + order.getRequestId() + ",'"
                        + order.getName() + "',"
                        + order.getQuantity() + ","
                        + order.getPrice() + ");";
                statement.execute(query);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Drop database.
     */
    static void dropDatabase() {
        try {
            statement.execute("DROP TABLE orders");
        } catch (SQLException e) {
//            e.printStackTrace();
        }
    }

    // Report

    /**
     * Show total orders.
     */
    static void showTotalOrders() {
        try {
            resultSet = statement.executeQuery(
                    "SELECT SUM(total) 'summary'"
                            + "FROM ("
                            + "SELECT COUNT(DISTINCT RequestId) 'total' "
                            + "FROM orders "
                            + "GROUP BY ClientId);");
            switch (toFile) {
                case "csv":
                    try {
                        csvReport.showTotalOrders(resultSet.getInt("summary"));
                        csvReport.getCsvWriter().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    onScreenReport.showTotalOrders(resultSet.getInt("summary"));
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show total orders.
     *
     * @param client the client
     */
    static void showTotalOrders(final String client) {
        try {
            resultSet = statement.executeQuery(
                    "SELECT COUNT(DISTINCT RequestId) 'summary'"
                            + "FROM orders "
                            + "WHERE ClientId = \"" + client + "\";");
            switch (toFile) {
                case "csv":
                    try {
                        csvReport.showTotalOrders(resultSet.getInt("summary"), client);
                        csvReport.getCsvWriter().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    onScreenReport.showTotalOrders(resultSet.getInt("summary"), client);
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show total value.
     */
    static void showTotalValue() {
        try {
            resultSet = statement.executeQuery(
                    "SELECT ROUND(SUM(Quantity * Price),2) 'total' "
                            + "FROM orders;");
            switch (toFile) {
                case "csv":
                    try {
                        csvReport.showTotalValue(resultSet.getDouble("total"));
                        csvReport.getCsvWriter().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    onScreenReport.showTotalValue(resultSet.getDouble("total"));
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show total value.
     *
     * @param client the client
     */
    static void showTotalValue(final String client) {
        try {
            resultSet = statement.executeQuery(
                    "SELECT ROUND(SUM(Quantity * Price),2) 'total' "
                            + "FROM orders "
                            + "WHERE ClientId =\"" + client + "\";");
            switch (toFile) {
                case "csv":
                    try {
                        csvReport.showTotalValue(resultSet.getDouble("total"), client);
                        csvReport.getCsvWriter().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    onScreenReport.showTotalValue(resultSet.getDouble("total"), client);
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show all orders.
     */
    static void showAllOrders() {
        try {
            resultSet = statement.executeQuery(
                    "SELECT ClientId, RequestId, Name, Quantity, Price "
                            + "FROM orders GROUP BY ClientId, RequestId, "
                            + "Name, Quantity, Price;");
            listAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show all orders.
     *
     * @param client the client
     */
    static void showAllOrders(final String client) {
        try {
            resultSet = statement.executeQuery(
                    "SELECT ClientId, RequestId, Name, Quantity, Price\n"
                            + "FROM orders "
                            + "WHERE ClientId = \"" + client + "\" "
                            + "GROUP BY ClientId, RequestId, "
                            + "Name, Quantity, Price;");
            listAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Exclusive method for listing orders in reports.
     *
     * @throws SQLException when wrongly parsed data.
     */
    private static void listAll() throws SQLException {
        switch (toFile) {
            case "csv":
                try {
                    csvReport.showAllOrders(false, "", 0, "", 0, 0);
                    while (resultSet.next()) {
                        csvReport.showAllOrders(true,
                                resultSet.getString("ClientId"),
                                resultSet.getLong("RequestId"),
                                resultSet.getString("Name"),
                                resultSet.getInt("Quantity"),
                                resultSet.getDouble("Price"));
                    }
                    csvReport.getCsvWriter().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
                onScreenReport.showAllOrders(false, "", 0, "", 0, 0);
                while (resultSet.next()) {
                    onScreenReport.showAllOrders(true,
                            resultSet.getString("ClientId"),
                            resultSet.getLong("RequestId"),
                            resultSet.getString("Name"),
                            resultSet.getInt("Quantity"),
                            resultSet.getDouble("Price"));
                }
                break;
        }
    }

    /**
     * Show avg value order.
     */
    static void showAvgValueOrder() {
        try {
            resultSet = statement.executeQuery(
                    "SELECT ROUND(AVG(total),2) 'totalAvg' "
                            + "FROM (SELECT ClientId, RequestId, "
                            + "SUM(Quantity * Price) 'total' "
                            + "FROM orders "
                            + "WHERE Quantity IS NOT 0 "
                            + "AND Price IS NOT 0 "
                            + "GROUP BY CLientId, RequestId);");
            switch (toFile) {
                case "csv":
                    try {
                        csvReport.showAvgValueOrder(resultSet.getDouble("totalAvg"));
                        csvReport.getCsvWriter().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    onScreenReport.showAvgValueOrder(resultSet.getDouble("totalAvg"));
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show avg value order.
     *
     * @param client the client
     */
    static void showAvgValueOrder(final String client) {
        try {
            resultSet = statement.executeQuery(
                    "SELECT ROUND(AVG(total),2) 'totalAvg' "
                            + "FROM (SELECT ClientId, RequestId, "
                            + "SUM(Quantity * Price) 'total' "
                            + "FROM orders "
                            + "WHERE ClientId = \"" + client + "\" "
                            + "AND Quantity IS NOT 0 "
                            + "AND Price IS NOT 0 "
                            + "GROUP BY CLientId, RequestId);");
            switch (toFile) {
                case "csv":
                    try {
                        csvReport.showAvgValueOrder(resultSet.getDouble("totalAvg"), client);
                        csvReport.getCsvWriter().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    onScreenReport.showAvgValueOrder(resultSet.getDouble("totalAvg"), client);
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
