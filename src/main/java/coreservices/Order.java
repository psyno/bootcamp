package coreservices;

/**
 * The type Order.
 */
public class Order {
    /**
     * Maximum length of order name.
     */
    private static final int MAX_NAME_LENGTH = 255;
    /**
     * Maximum length of clientId.
     */
    private static final int MAX_ID_LENGTH = 6;
    /**
     * The type int database id.
     */
    private int id;
    /**
     * The type String clientId. Length restricted by {@link #MAX_ID_LENGTH}.
     */
    private String clientId;
    /**
     * The type long requestId.
     */
    private long requestId;
    /**
     * The type String name. Length restricted by {@link #MAX_NAME_LENGTH}.
     */
    private String name;
    /**
     * The type int quantity.
     */
    private int quantity;
    /**
     * The type double price.
     */
    private double price;

    /**
     * Instantiates a new Order.
     *
     * @param i the database id
     * @param c the client id
     * @param r the request id
     * @param n the name
     * @param q the quantity
     * @param p the price
     */
    public Order(final String i, final String c, final String r,
                 final String n, final String q, final String p) {
        setId(i);
        setClientId(c);
        setRequestId(r);
        setName(n);
        setQuantity(q);
        setPrice(p);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    final int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param i the database id
     */
    void setId(final int i) {
        this.id = i;
    }

    /**
     * Sets id.
     *
     * @param i the database id
     */
    private void setId(final String i) {
        this.id = Integer.parseInt(i);
    }

    /**
     * Gets client id.
     *
     * @return the client id
     */
    final String getClientId() {
        return clientId;
    }

    /**
     * Sets clientId.
     *
     * @param c the client id
     */
    private void setClientId(final String c) {
        if (c.length() <= MAX_ID_LENGTH
                && !c.contains(" ")
                && !c.equals("NoN")
                && c.length() > 0) {
            this.clientId = c;
        } else {
            System.out.println("Invalid ClientId; "
                    + "expected <7 chars, "
                    + "without whitespaces, "
                    + "at least 1 character; "
                    + "Field clientId setted to default");
            this.clientId = "NoN";
        }
    }

    /**
     * Gets request id.
     *
     * @return the request id
     */
    final long getRequestId() {
        return requestId;
    }

    /**
     * Sets request id.
     *
     * @param r the request id
     */
    private void setRequestId(final String r) {
        try {
            this.requestId = Long.parseLong(r);
        } catch (Exception e) {
            System.out.println("Invalid input data type; "
                    + "Field RequestId setted to default");
            this.requestId = 0;
        }
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    final String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param n the name
     */
    private void setName(final String n) {
        if (n.length() <= MAX_NAME_LENGTH && n.length() > 0) {
            this.name = n;
        } else {
            System.out.println("Name value too short or too long; "
                    + "Field Name setted to default");
            this.name = "NoN";
        }
    }

    /**
     * Gets quantity.
     *
     * @return the quantity
     */
    final int getQuantity() {
        return quantity;
    }

    /**
     * Sets quantity. Exclusively non-private since needed in other package.
     *
     * @param q the quantity
     */
    void setQuantity(final String q) {
        try {
            int tmpQuantity = Integer.parseInt(q);
            if (tmpQuantity > 0) {
                this.quantity = tmpQuantity;
            } else {
                System.out.println("Quantity can't be zero or negative; "
                        + "Field Quantity setted to default");
                this.quantity = 0;
            }
        } catch (NumberFormatException e) {
            System.out.println("Invalid input data type; "
                    + "Field Quantity setted to default");
            this.quantity = 0;
        }
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    final double getPrice() {
        return price;
    }

    /**
     * Sets price. Exclusively non-private since needed in other package.
     *
     * @param p the price
     */
    void setPrice(final String p) {
        try {
            double tmpPrice = Double.parseDouble(p);
            if (tmpPrice > 0) {
                this.price = Math.round(tmpPrice * 100.0) / 100.0;
            } else {
                System.out.println("Price can't be zero or negative; "
                        + "Field Price setted to default");
            }
        } catch (Exception e) {
            System.out.println("Invalid input data type; "
                    + "Field Price setted to default");
            this.price = 0;
        }
    }

    @Override
    public final String toString() {
        return "\nOrder{"
                + "id=" + id
                + ", clientId='" + clientId + '\''
                + ", requestId=" + requestId
                + ", name='" + name + '\''
                + ", quantity=" + quantity
                + ", price=" + price + '}';
    }

    /**
     * To string with separator string.
     *
     * @return the string
     */
    public final String toStringWithSeparator() {
        return "Order{"
                + "id=" + id
                + ", clientId='" + clientId + '\''
                + ", requestId=" + requestId
                + ", name='" + name + '\''
                + ", quantity=" + quantity
                + ", price=" + price + '}'
                + "\n----------------------------";
    }

    /**
     * Gets max name length.
     *
     * @return the max name length
     */
    static int getMaxNameLength() {
        return MAX_NAME_LENGTH;
    }

    /**
     * Gets maximum clientId length.
     *
     * @return the max clientId length
     */
    static int getMaxIdLength() {
        return MAX_ID_LENGTH;
    }
}
