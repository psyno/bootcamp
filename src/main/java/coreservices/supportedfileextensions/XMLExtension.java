package coreservices.supportedfileextensions;

import coreservices.Order;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.File;
import java.util.ArrayList;

/**
 * The type Xml extension.
 */
public class XMLExtension implements Extension {
    /**
     * Object to be readed from.
     */
    private File xmlFile;
    /**
     * An ArrayList that contains readed orders.
     */
    private ArrayList<Order> orders;

    /**
     * Instantiates a new Xml extension.
     *
     * @param xml the xml file
     */
    public XMLExtension(final File xml) {
        this.xmlFile = xml;
        this.orders = new ArrayList();
    }

    @Override
    public final ArrayList<Order> readFromFile() {

        NodeList nList = null;

        try {
            DocumentBuilderFactory dbFactory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            nList = doc.getElementsByTagName("request");
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;
                String[] readed = new String[5];
                try {
                    readed[0] = eElement.getElementsByTagName("clientId")
                            .item(0).getTextContent();
                } catch (NullPointerException e) {
                    readed[0] = "NoN";
                }
                try {
                    readed[1] = eElement.getElementsByTagName("requestId")
                            .item(0).getTextContent();
                } catch (NullPointerException e) {
                    readed[1] = "0";
                }
                try {
                    readed[2] = eElement.getElementsByTagName("name")
                            .item(0).getTextContent();
                } catch (NullPointerException e) {
                    readed[2] = "NoN";
                }
                try {
                    readed[3] = eElement.getElementsByTagName("quantity")
                            .item(0).getTextContent();
                } catch (NullPointerException e) {
                    readed[3] = "0";
                }
                try {
                    readed[4] = eElement.getElementsByTagName("price")
                            .item(0).getTextContent();
                } catch (NullPointerException e) {
                    readed[4] = "0";
                }
                this.orders.add(new Order(Integer.toString(temp),
                        readed[0], readed[1], readed[2],
                        readed[3], readed[4]));
                System.out.println(orders.get(temp).toStringWithSeparator());
            }
        }
        return orders;
    }
}
