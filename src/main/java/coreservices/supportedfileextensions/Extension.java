package coreservices.supportedfileextensions;

import coreservices.Order;

import java.util.ArrayList;

/**
 * The interface Extension for supported files to read.
 */
interface Extension {
    /**
     * An ArrayList that contains orders;
     */
    ArrayList<Order> orders = new ArrayList<>();

    /**
     * Read from file with specific extension and
     * return results as ArrayList.
     *
     * @return the array list
     */
    ArrayList<Order> readFromFile();
}
