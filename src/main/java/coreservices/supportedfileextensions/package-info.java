/**
 * Subpackage of coreservices that provides supported file extensions.
 */

package coreservices.supportedfileextensions;