package coreservices.supportedfileextensions;

import com.opencsv.CSVReader;
import coreservices.Order;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * The type Csv extension.
 */
public class CSVExtension implements Extension {
    /**
     * Object to be readed from.
     */
    private final File csvFile;
    /**
     * An ArrayList that contains readed orders.
     */
    private final ArrayList<Order> orders;

    /**
     * Instantiates a new Csv extension.
     * Initiates ArrayList orders.
     *
     * @param csv the csv file
     */
    public CSVExtension(final File csv) {
        this.csvFile = csv;
        this.orders = new ArrayList<>();
    }

    @Override
    public final ArrayList<Order> readFromFile() {
        try {
            CSVReader csvReader = new CSVReader(
                    new FileReader(csvFile.getName()), ',');
            String[] line;
            try {
                csvReader.readNext();
                line = csvReader.readNext();
                int i = 0;
                while (line != null) {
                    this.orders.add(new Order(
                            Integer.toString(i),
                            line[0],
                            line[1],
                            line[2],
                            line[3],
                            line[4]));
                    System.out.println(orders.get(i).toStringWithSeparator());
                    i++;
                    line = csvReader.readNext();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return orders;
    }
}
