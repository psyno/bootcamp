/**
 * Package created for Core Services BootCamp
 *
 * Package designed to process XML and CSV files with dedicated structure.
 * Reports can be shown on screen or saved to CSV file.
 *
 * @author pawelsynowiec
 */

package coreservices;