#!/bin/bash

echo "Opening App..."
eval java -cp target:lib/opencsv-4.2.jar:lib/junit-4.11.jar:lib/sqlite-jdbc-3.23.1.jar:lib/commons-lang3-3.7.jar:target/bootcamp-1.0-SNAPSHOT.jar coreservices.App "$@"
echo "App closed."